#!/bin/bash

cat << __EOF__

######################### WARNING ###########################

This script viciously cleans up everything from docker:

 - All running/non-running containers.
 - All images.
 - All volumes.

Sure? (yes)
__EOF__

read inp

[ "$inp" != "yes" ] && exit 1 ;

docker ps -q | xargs docker kill 
docker ps -aq | xargs docker rm -f
docker images -aq | xargs docker rmi -f
docker volume ls -q | xargs docker volume rm 


